use eframe::egui;

use crate::alphabet::ALPHABET;
use crate::letter::Letter;
use crate::puzzle;
use crate::question::{Orientation, Question};

pub struct CrossWord {
    pub width: usize,
    pub height: usize,
    pub grid: Vec<Vec<Letter>>,
    pub give_focus_to: Option<(usize, usize)>,
    pub questions: Vec<Question>,
}

impl CrossWord {
    pub fn from_json(filepath: &str) -> Result<Self, puzzle::CrossError> {
        Self::initialize(&puzzle::parse_json(filepath)?)
    }

    pub fn initialize(questions: &[Question]) -> Result<Self, puzzle::CrossError> {
        let width = questions.iter().map(|q| q.get_max_x() + 1).max().unwrap();
        let height = questions.iter().map(|q| q.get_max_y() + 1).max().unwrap();

        let mut grid: Vec<Vec<Letter>> = (0..width)
            .map(|_| (0..height).map(|_| Letter::default()).collect())
            .collect();
        for q in questions {
            grid[q.pos.0][q.pos.1].idx = Some(q.idx);
            for (i, c) in q.answer.iter().enumerate() {
                let (x, y) = match q.orientation {
                    Orientation::Horizontal => (q.pos.0 + i, q.pos.1),
                    Orientation::Vertical => (q.pos.0, q.pos.1 + i),
                };
                grid[x][y].inserted_char = Some(c.to_ascii_uppercase());
                if grid[x][y].desired_char.is_some() {
                    if grid[x][y].desired_char != Some(c.to_ascii_uppercase()) {
                        return Err(puzzle::CrossError::Other);
                    }
                } else {
                    grid[x][y].desired_char = Some(c.to_ascii_uppercase());
                }
            }
        }
        Ok(Self {
            width,
            height,
            grid,
            give_focus_to: None,
            questions: questions.to_vec(),
        })
    }

    pub fn finished(&self) -> bool {
        for row in &self.grid {
            for l in row {
                if l.blocked() {
                    continue;
                }
                if l.inserted_char != l.desired_char {
                    return false;
                }
            }
        }
        true
    }

    pub fn vertical_questions(&self) -> impl Iterator<Item = &Question> {
        self.filter_questions(Orientation::Vertical)
    }

    pub fn horizontal_questions(&self) -> impl Iterator<Item = &Question> {
        self.filter_questions(Orientation::Horizontal)
    }

    fn filter_questions(&self, kind: Orientation) -> impl Iterator<Item = &Question> {
        self.questions.iter().filter(move |q| q.orientation == kind)
    }

    pub fn draw_and_update_letter(
        &mut self,
        ui: &mut egui::Ui,
        i: usize,
        j: usize,
        box_size: [f32; 2],
        give_focus_to_next_frame: &mut Option<(usize, usize)>,
    ) {
        let res = ui.allocate_response(egui::Vec2::from(box_size), egui::Sense::click());
        if self.grid[i][j].blocked() {
            // Black box
            ui.painter().rect_filled(
                res.rect,
                0.0,
                ui.visuals().widgets.noninteractive.fg_stroke.color,
            );
        } else {
            // Background
            ui.painter().rect_filled(
                res.rect,
                0.0,
                if self.grid[i][j].locked {
                    ui.visuals().weak_text_color()
                } else {
                    ui.visuals().faint_bg_color
                },
            );
            // Letter
            ui.painter().text(
                res.rect.center(),
                egui::Align2::CENTER_CENTER,
                self.grid[i][j].get_string(),
                egui::FontId::monospace(box_size[0] * 0.8), // 23.0
                ui.visuals().widgets.active.fg_stroke.color,
            );
            // Idx
            if let Some(idx) = self.grid[i][j].idx {
                ui.painter().text(
                    res.rect.left_top(),
                    egui::Align2::LEFT_TOP,
                    idx.to_string(),
                    egui::FontId::monospace(box_size[0] * 0.35), // 15.0
                    ui.visuals().widgets.active.fg_stroke.color,
                );
            }
        }
        // Outline
        if res.has_focus() {
            let stroke_weight = box_size[0] * 0.15;
            ui.painter().rect_stroke(
                res.rect.expand(-stroke_weight / 2.0),
                0.0,
                (stroke_weight, ui.visuals().widgets.active.fg_stroke.color),
            );
        } else {
            ui.painter().rect_stroke(
                res.rect,
                0.0,
                (1.0, ui.visuals().widgets.active.fg_stroke.color),
            );
        }
        if let Some((x, y)) = self.give_focus_to {
            if (i, j) == (x, y) {
                // println!("Requesting focus from {:?}", (i, j));
                res.request_focus();
                self.give_focus_to = None;
            }
        }
        if res.clicked() {
            res.request_focus()
        }

        if res.has_focus() {
            if !self.grid[i][j].locked {
                for (k, ch) in ALPHABET {
                    if ui.input().key_pressed(k) {
                        self.grid[i][j].inserted_char = Some(ch);
                    }
                }
                if ui.input().key_pressed(egui::Key::Backspace) {
                    self.grid[i][j].inserted_char = None;
                }
            }

            if ui.input().key_pressed(egui::Key::Space) {
                self.grid[i][j].toggle_lock();
            }

            if give_focus_to_next_frame.is_none() {
                if ui.input().key_pressed(egui::Key::ArrowLeft) {
                    if i > 0 {
                        *give_focus_to_next_frame = Some((i - 1, j));
                    }
                }
                if ui.input().key_pressed(egui::Key::ArrowRight) {
                    if i < self.width - 1 {
                        *give_focus_to_next_frame = Some((i + 1, j));
                    }
                }
                if ui.input().key_pressed(egui::Key::ArrowUp) {
                    if j > 0 {
                        *give_focus_to_next_frame = Some((i, j - 1));
                    }
                }
                if ui.input().key_pressed(egui::Key::ArrowDown) {
                    if j < self.height - 1 {
                        *give_focus_to_next_frame = Some((i, j + 1));
                    }
                }
            }
        }
    }

    pub fn draw_and_update(&mut self, ui: &mut egui::Ui) {
        let mut give_focus_to_next_frame = None;

        let full_width = ui.available_width();
        let full_height = ui.available_height();
        let box_size = {
            let box_width = (full_width * 0.65) / self.width as f32;
            let box_height = (full_height * 0.65) / self.height as f32;
            [box_height.min(box_width), box_height.min(box_width)]
        };
        let padding_width = (full_width - box_size[0] * self.width as f32) / 2.;
        ui.scope(|ui| {
            ui.style_mut().spacing.item_spacing = eframe::epaint::Vec2::new(0., 0.);
            for j in 0..self.height {
                ui.with_layout(
                    egui::Layout::left_to_right().with_cross_align(egui::Align::TOP),
                    |ui| {
                        ui.allocate_space(eframe::epaint::Vec2::new(padding_width, 0.));
                        for i in 0..self.width {
                            self.draw_and_update_letter(
                                ui,
                                i,
                                j,
                                box_size,
                                &mut give_focus_to_next_frame,
                            )
                        }
                    },
                );
            }
        });
        self.give_focus_to = give_focus_to_next_frame;
    }
}

impl Default for CrossWord {
    fn default() -> Self {
        if let Ok(crossword) = Self::initialize(&puzzle::get_questions()) {
            crossword
        } else {
            panic!("default constructor for CrossWord failed")
        }
    }
}
