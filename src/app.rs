use eframe::{
    egui::{self, RichText},
    epi,
};

use crate::crossword::CrossWord;
use crate::question::Question;

pub struct CrossWordApp {
    crossword: Option<CrossWord>,
}

impl CrossWordApp {
    pub fn _from_json(filepath: &str) -> Self {
        if let Ok(crossword) = CrossWord::from_json(filepath) {
            Self {
                crossword: Some(crossword),
            }
        } else {
            Self { crossword: None }
        }
    }

    pub fn _initialize(questions: &[Question]) -> Self {
        if let Ok(crossword) = CrossWord::initialize(questions) {
            Self {
                crossword: Some(crossword),
            }
        } else {
            Self { crossword: None }
        }
    }

    pub fn new() -> Self {
        Self { crossword: None }
    }
}

impl Default for CrossWordApp {
    fn default() -> Self {
        Self {
            crossword: Some(CrossWord::default()),
        }
    }
}

impl epi::App for CrossWordApp {
    fn setup(
        &mut self,
        ctx: &egui::Context,
        _frame: &epi::Frame,
        _storage: Option<&dyn epi::Storage>,
    ) {
        ctx.set_visuals(egui::Visuals::light());
        let mut style = (*ctx.style()).clone();
        if let Some(font) = style.text_styles.get_mut(&egui::TextStyle::Heading) {
            font.size = 45.;
        }
        if let Some(font) = style.text_styles.get_mut(&egui::TextStyle::Body) {
            font.size = 25.;
        }
        if let Some(font) = style.text_styles.get_mut(&egui::TextStyle::Button) {
            font.size = 25.;
        }
        if let Some(font) = style.text_styles.get_mut(&egui::TextStyle::Monospace) {
            font.size = 25.;
        }
        ctx.set_style(style);
    }

    fn update(&mut self, ctx: &egui::Context, frame: &eframe::epi::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            if let Some(crossword) = &mut self.crossword {
                let mut go_back = false;
                ui.horizontal(|ui| {
                    if ui.button(RichText::from("<").monospace()).clicked() {
                        go_back = true;
                    }
                    if ui.button(RichText::from("X").monospace()).clicked() {
                        frame.quit();
                    }
                });

                crossword.draw_and_update(ui);

                if crossword.finished() {
                    ui.vertical_centered(|ui| {
                        ui.heading(RichText::from("Finished!").color(egui::Color32::GREEN));
                    });
                }

                ui.columns(2, |columns| {
                    columns[0].group(|ui| {
                        egui::ScrollArea::vertical()
                            .id_source("Horizontal clues")
                            .show(ui, |ui| {
                                ui.vertical_centered(|ui| {
                                    ui.heading("Horzzontal");
                                    for q in crossword.horizontal_questions() {
                                        ui.label(format!("{}: {}", q.idx, q.clue));
                                    }
                                });
                            });
                    });
                    columns[1].group(|ui| {
                        egui::ScrollArea::vertical()
                            .id_source("Vertical clues")
                            .show(ui, |ui| {
                                ui.vertical_centered(|ui| {
                                    ui.heading("Vertical");
                                    for q in crossword.vertical_questions() {
                                        ui.label(format!("{}: {}", q.idx, q.clue));
                                    }
                                });
                            });
                    });
                });
                if go_back {
                    self.crossword = None;
                }
            } else {
                ui.vertical_centered(|ui| {
                    ui.heading("No crossword selected");
                });

                ui.with_layout(
                    egui::Layout::left_to_right()
                        .with_cross_align(egui::Align::Center)
                        .with_main_justify(true),
                    |ui| {
                        let mut picked_path = None;
                        if ui.button("Import crossword from file...").clicked() {
                            if let Some(path) = rfd::FileDialog::new()
                                .add_filter("json", &["json"])
                                .set_directory("data/nyt_crosswords")
                                .pick_file()
                            {
                                picked_path = Some(path);
                            }
                        }
                        if let Some(path) = picked_path {
                            self.crossword = match CrossWord::from_json(&path.display().to_string())
                            {
                                Ok(crossword) => Some(crossword),
                                Err(_) => None,
                            }
                        }
                    },
                );
            }
        });
    }

    fn name(&self) -> &str {
        "CrossWords"
    }
}
