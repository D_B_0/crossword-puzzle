mod alphabet;
mod app;
mod crossword;
mod letter;
mod puzzle;
mod question;

fn main() {
    eframe::run_native(
        // Box::new(app::CrossWordApp::from_json("data/nyt.json")),
        Box::new(app::CrossWordApp::new()),
        eframe::NativeOptions::default(),
    );
}
