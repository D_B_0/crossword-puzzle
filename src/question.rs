#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Orientation {
    Vertical,
    Horizontal,
}

#[derive(Debug, Clone)]
pub struct Question {
    pub idx: usize,
    pub clue: String,
    pub answer: Vec<char>,
    pub orientation: Orientation,
    pub pos: (usize, usize),
}

impl Question {
    pub fn get_max_x(&self) -> usize {
        match self.orientation {
            Orientation::Vertical => self.pos.0,
            Orientation::Horizontal => self.pos.0 + self.answer.len() - 2,
        }
    }

    pub fn get_max_y(&self) -> usize {
        match self.orientation {
            Orientation::Horizontal => self.pos.1,
            Orientation::Vertical => self.pos.1 + self.answer.len() - 2,
        }
    }
}
