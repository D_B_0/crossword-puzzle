use std::fs;

use crate::question::{Orientation, Question};

pub enum CrossError {
    Other,
}

impl From<std::io::Error> for CrossError {
    fn from(_: std::io::Error) -> Self {
        Self::Other
    }
}

impl From<json::Error> for CrossError {
    fn from(_: json::Error) -> Self {
        Self::Other
    }
}

fn json_array_to_vec(json_array: &json::JsonValue) -> Result<Vec<json::JsonValue>, CrossError> {
    if !json_array.is_array() {
        return Err(CrossError::Other);
    }
    let mut res = Vec::with_capacity(json_array.len());
    for i in 0..json_array.len() {
        res.push(json_array[i].clone());
    }
    Ok(res)
}

pub fn parse_json(filename: &str) -> Result<Vec<Question>, CrossError> {
    let parsed = json::parse(&fs::read_to_string(filename)?)?;
    let width = parsed["size"]["cols"].as_usize().unwrap();
    // let height = parsed["size"]["rows"].as_usize().unwrap();
    let horiz_questions: Vec<(usize, String)> = json_array_to_vec(&parsed["clues"]["across"])?
        .iter()
        .map(|j| {
            let (idx, clue) = j.as_str().unwrap().split_once(". ").unwrap();
            (idx.parse::<usize>().unwrap(), clue.to_owned())
        })
        .collect();
    let horiz_answers: Vec<Vec<char>> = json_array_to_vec(&parsed["answers"]["across"])?
        .iter()
        .map(|j| j.as_str().unwrap().chars().collect())
        .collect();
    if horiz_questions.len() != horiz_answers.len() {
        return Err(CrossError::Other);
    }

    let vert_questions: Vec<(usize, String)> = json_array_to_vec(&parsed["clues"]["down"])?
        .iter()
        .map(|j| {
            let (idx, clue) = j.as_str().unwrap().split_once(". ").unwrap();
            (idx.parse::<usize>().unwrap(), clue.to_owned())
        })
        .collect();
    let vert_answers: Vec<Vec<char>> = json_array_to_vec(&parsed["answers"]["down"])?
        .iter()
        .map(|j| j.as_str().unwrap().chars().collect())
        .collect();
    if vert_questions.len() != vert_answers.len() {
        return Err(CrossError::Other);
    }

    let idx_grid: Vec<usize> = json_array_to_vec(&parsed["gridnums"])?
        .iter()
        .map(|j| j.as_usize().unwrap())
        .collect();

    let max_idx = *idx_grid.iter().max().unwrap();

    let mut map_idx_to_pos: Vec<(usize, usize)> = Vec::with_capacity(max_idx + 1);
    for i in 1..=max_idx {
        let grid_index = idx_grid.iter().position(|&idx| idx == i).unwrap();
        let x = grid_index % width;
        let y = grid_index / width;
        map_idx_to_pos.push((x, y));
    }

    let mut questions: Vec<Question> = Vec::new();

    for i in 0..horiz_questions.len() {
        let (idx, clue) = horiz_questions[i].clone();
        let answer = horiz_answers[i].clone();
        let pos = map_idx_to_pos[idx - 1];

        questions.push(Question {
            idx,
            clue,
            answer,
            pos,
            orientation: Orientation::Horizontal,
        });
    }
    for i in 0..vert_questions.len() {
        let (idx, clue) = vert_questions[i].clone();
        let answer = vert_answers[i].clone();
        let pos = map_idx_to_pos[idx - 1];

        questions.push(Question {
            idx,
            clue,
            answer,
            pos,
            orientation: Orientation::Vertical,
        });
    }
    Ok(questions)
}

pub fn get_questions() -> Vec<Question> {
    // Crosswowrd 28559 on https://www.boatloadpuzzles.com/playcrossword
    vec![
        Question {
            idx: 1,
            clue: String::from("It follows Oct."),
            orientation: Orientation::Horizontal,
            answer: vec!['n', 'o', 'v'],
            pos: (0, 0),
        },
        Question {
            idx: 4,
            clue: String::from("Exclamation of woe"),
            orientation: Orientation::Horizontal,
            answer: vec!['a', 'l', 'a', 's'],
            pos: (4, 0),
        },
        Question {
            idx: 8,
            clue: String::from("Guns an engine"),
            orientation: Orientation::Horizontal,
            answer: vec!['r', 'e', 'v', 's'],
            pos: (9, 0),
        },
        Question {
            idx: 12,
            clue: String::from("A Gershwin"),
            orientation: Orientation::Horizontal,
            answer: vec!['i', 'r', 'a'],
            pos: (0, 1),
        },
        Question {
            idx: 13,
            clue: String::from("Wealthy"),
            orientation: Orientation::Horizontal,
            answer: vec!['r', 'i', 'c', 'h'],
            pos: (4, 1),
        },
        Question {
            idx: 14,
            clue: String::from("Outlet"),
            orientation: Orientation::Horizontal,
            answer: vec!['e', 'x', 'i', 't'],
            pos: (9, 1),
        },
        Question {
            idx: 15,
            clue: String::from("Indulgence"),
            orientation: Orientation::Horizontal,
            answer: vec!['l', 'e', 'n', 'i', 'e', 'n', 'c', 'e'],
            pos: (0, 2),
        },
        Question {
            idx: 17,
            clue: String::from("Whiten"),
            orientation: Orientation::Horizontal,
            answer: vec!['f', 'a', 'd', 'e'],
            pos: (9, 2),
        },
        Question {
            idx: 18,
            clue: String::from("Picture"),
            orientation: Orientation::Horizontal,
            answer: vec!['i', 'm', 'a', 'g', 'e'],
            pos: (2, 3),
        },
        Question {
            idx: 19,
            clue: String::from("Director Spike ____"),
            orientation: Orientation::Horizontal,
            answer: vec!['l', 'e', 'e'],
            pos: (10, 3),
        },
        Question {
            idx: 20,
            clue: String::from("Composed"),
            orientation: Orientation::Horizontal,
            answer: vec!['c', 'a', 'l', 'm'],
            pos: (0, 4),
        },
        Question {
            idx: 22,
            clue: String::from("Phone company employee"),
            orientation: Orientation::Horizontal,
            answer: vec!['o', 'p', 'e', 'r', 'a', 't', 'o', 'r'],
            pos: (5, 4),
        },
        Question {
            idx: 26,
            clue: String::from("Lubricated"),
            orientation: Orientation::Horizontal,
            answer: vec!['o', 'i', 'l', 'e', 'd'],
            pos: (0, 5),
        },
        Question {
            idx: 28,
            clue: String::from("Tightly stretched"),
            orientation: Orientation::Horizontal,
            answer: vec!['t', 'a', 'u', 't'],
            pos: (6, 5),
        },
        Question {
            idx: 29,
            clue: String::from("Jupiter, e.g."),
            orientation: Orientation::Horizontal,
            answer: vec!['p', 'l', 'a', 'n', 'e', 't'],
            pos: (0, 6),
        },
        Question {
            idx: 31,
            clue: String::from("Except if"),
            orientation: Orientation::Horizontal,
            answer: vec!['u', 'n', 'l', 'e', 's', 's'],
            pos: (7, 6),
        },
        Question {
            idx: 35,
            clue: String::from("Warbled"),
            orientation: Orientation::Horizontal,
            answer: vec!['s', 'a', 'n', 'g'],
            pos: (3, 7),
        },
        Question {
            idx: 37,
            clue: String::from("Parties"),
            orientation: Orientation::Horizontal,
            answer: vec!['g', 'a', 'l', 'a', 's'],
            pos: (8, 7),
        },
        Question {
            idx: 38,
            clue: String::from("Used a compass"),
            orientation: Orientation::Horizontal,
            answer: vec!['o', 'r', 'i', 'e', 'n', 't', 'e', 'd'],
            pos: (0, 8),
        },
        Question {
            idx: 42,
            clue: String::from("Back of the neck"),
            orientation: Orientation::Horizontal,
            answer: vec!['n', 'a', 'p', 'e'],
            pos: (9, 8),
        },
        Question {
            idx: 43,
            clue: String::from("Sleeping place"),
            orientation: Orientation::Horizontal,
            answer: vec!['b', 'e', 'd'],
            pos: (0, 9),
        },
        Question {
            idx: 44,
            clue: String::from("Salamanders"),
            orientation: Orientation::Horizontal,
            answer: vec!['n', 'e', 'w', 't', 's'],
            pos: (6, 9),
        },
        Question {
            idx: 46,
            clue: String::from("At all times"),
            orientation: Orientation::Horizontal,
            answer: vec!['e', 'v', 'e', 'r'],
            pos: (0, 10),
        },
        Question {
            idx: 48,
            clue: String::from("Said again"),
            orientation: Orientation::Horizontal,
            answer: vec!['r', 'e', 'p', 'e', 'a', 't', 'e', 'd'],
            pos: (5, 10),
        },
        Question {
            idx: 51,
            clue: String::from("Garment juncture"),
            orientation: Orientation::Horizontal,
            answer: vec!['s', 'e', 'a', 'm'],
            pos: (0, 11),
        },
        Question {
            idx: 52,
            clue: String::from("Stratford-on-____"),
            orientation: Orientation::Horizontal,
            answer: vec!['a', 'v', 'o', 'n'],
            pos: (5, 11),
        },
        Question {
            idx: 53,
            clue: String::from("Clinging vine"),
            orientation: Orientation::Horizontal,
            answer: vec!['i', 'v', 'y'],
            pos: (10, 11),
        },
        Question {
            idx: 54,
            clue: String::from("House additions"),
            orientation: Orientation::Horizontal,
            answer: vec!['e', 'l', 'l', 's'],
            pos: (0, 12),
        },
        Question {
            idx: 55,
            clue: String::from("Actor ____ Damon"),
            orientation: Orientation::Horizontal,
            answer: vec!['m', 'a', 't', 't'],
            pos: (5, 12),
        },
        Question {
            idx: 56,
            clue: String::from("Middling grade"),
            orientation: Orientation::Horizontal,
            answer: vec!['c', 'e', 'e'],
            pos: (10, 12),
        },
        Question {
            idx: 1,
            clue: String::from("Zero"),
            orientation: Orientation::Vertical,
            answer: vec!['n', 'i', 'l'],
            pos: (0, 0),
        },
        Question {
            idx: 2,
            clue: String::from("Raw material"),
            orientation: Orientation::Vertical,
            answer: vec!['o', 'r', 'e'],
            pos: (1, 0),
        },
        Question {
            idx: 3,
            clue: String::from("Ice-cream flavor"),
            orientation: Orientation::Vertical,
            answer: vec!['v', 'a', 'n', 'i', 'l', 'l', 'a'],
            pos: (2, 0),
        },
        Question {
            idx: 4,
            clue: String::from("Territory"),
            orientation: Orientation::Vertical,
            answer: vec!['a', 'r', 'e', 'a'],
            pos: (4, 0),
        },
        Question {
            idx: 5,
            clue: String::from("Jargon"),
            orientation: Orientation::Vertical,
            answer: vec!['l', 'i', 'n', 'g', 'o'],
            pos: (5, 0),
        },
        Question {
            idx: 6,
            clue: String::from("Receive willingly"),
            orientation: Orientation::Vertical,
            answer: vec!['a', 'c', 'c', 'e', 'p', 't'],
            pos: (6, 0),
        },
        Question {
            idx: 7,
            clue: String::from("That lady"),
            orientation: Orientation::Vertical,
            answer: vec!['s', 'h', 'e'],
            pos: (7, 0),
        },
        Question {
            idx: 8,
            clue: String::from("Ump's kin"),
            orientation: Orientation::Vertical,
            answer: vec!['r', 'e', 'f'],
            pos: (9, 0),
        },
        Question {
            idx: 9,
            clue: String::from("Glorify"),
            orientation: Orientation::Vertical,
            answer: vec!['e', 'x', 'a', 'l', 't'],
            pos: (10, 0),
        },
        Question {
            idx: 10,
            clue: String::from("MTV feature"),
            orientation: Orientation::Vertical,
            answer: vec!['v', 'i', 'd', 'e', 'o'],
            pos: (11, 0),
        },
        Question {
            idx: 11,
            clue: String::from("Direct"),
            orientation: Orientation::Vertical,
            answer: vec!['s', 't', 'e', 'e', 'r'],
            pos: (12, 0),
        },
        Question {
            idx: 16,
            clue: String::from("Huge"),
            orientation: Orientation::Vertical,
            answer: vec!['i', 'm', 'm', 'e', 'n', 's', 'e'],
            pos: (3, 2),
        },
        Question {
            idx: 20,
            clue: String::from("Police officer"),
            orientation: Orientation::Vertical,
            answer: vec!['c', 'o', 'p'],
            pos: (0, 4),
        },
        Question {
            idx: 21,
            clue: String::from("Feel sick"),
            orientation: Orientation::Vertical,
            answer: vec!['a', 'i', 'l'],
            pos: (1, 4),
        },
        Question {
            idx: 23,
            clue: String::from("____ de toilette"),
            orientation: Orientation::Vertical,
            answer: vec!['e', 'a', 'u'],
            pos: (7, 4),
        },
        Question {
            idx: 24,
            clue: String::from("Ladder step"),
            orientation: Orientation::Vertical,
            answer: vec!['r', 'u', 'n', 'g'],
            pos: (8, 4),
        },
        Question {
            idx: 25,
            clue: String::from("Georgia city"),
            orientation: Orientation::Vertical,
            answer: vec!['a', 't', 'l', 'a', 'n', 't', 'a'],
            pos: (9, 4),
        },
        Question {
            idx: 27,
            clue: String::from("Singer ____ Martin"),
            orientation: Orientation::Vertical,
            answer: vec!['d', 'e', 'a', 'n'],
            pos: (4, 5),
        },
        Question {
            idx: 30,
            clue: String::from("Blasting letters"),
            orientation: Orientation::Vertical,
            answer: vec!['t', 'n', 't'],
            pos: (5, 6),
        },
        Question {
            idx: 32,
            clue: String::from("Rubber band"),
            orientation: Orientation::Vertical,
            answer: vec!['e', 'l', 'a', 's', 't', 'i', 'c'],
            pos: (10, 6),
        },
        Question {
            idx: 33,
            clue: String::from("Plant juice"),
            orientation: Orientation::Vertical,
            answer: vec!['s', 'a', 'p'],
            pos: (11, 6),
        },
        Question {
            idx: 34,
            clue: String::from("Compass dir."),
            orientation: Orientation::Vertical,
            answer: vec!['s', 's', 'e'],
            pos: (12, 6),
        },
        Question {
            idx: 36,
            clue: String::from("Swiss city"),
            orientation: Orientation::Vertical,
            answer: vec!['g', 'e', 'n', 'e', 'v', 'a'],
            pos: (6, 7),
        },
        Question {
            idx: 38,
            clue: String::from("Chubby"),
            orientation: Orientation::Vertical,
            answer: vec!['o', 'b', 'e', 's', 'e'],
            pos: (0, 8),
        },
        Question {
            idx: 39,
            clue: String::from("Make merry"),
            orientation: Orientation::Vertical,
            answer: vec!['r', 'e', 'v', 'e', 'l'],
            pos: (1, 8),
        },
        Question {
            idx: 40,
            clue: String::from("Best"),
            orientation: Orientation::Vertical,
            answer: vec!['i', 'd', 'e', 'a', 'l'],
            pos: (2, 8),
        },
        Question {
            idx: 41,
            clue: String::from("Bus station"),
            orientation: Orientation::Vertical,
            answer: vec!['d', 'e', 'p', 'o', 't'],
            pos: (7, 8),
        },
        Question {
            idx: 45,
            clue: String::from("Departed"),
            orientation: Orientation::Vertical,
            answer: vec!['w', 'e', 'n', 't'],
            pos: (8, 9),
        },
        Question {
            idx: 47,
            clue: String::from("Den and study (abbr.)"),
            orientation: Orientation::Vertical,
            answer: vec!['r', 'm', 's'],
            pos: (3, 10),
        },
        Question {
            idx: 48,
            clue: String::from("Ewe's mate"),
            orientation: Orientation::Vertical,
            answer: vec!['r', 'a', 'm'],
            pos: (5, 10),
        },
        Question {
            idx: 49,
            clue: String::from("First woman"),
            orientation: Orientation::Vertical,
            answer: vec!['e', 'v', 'e'],
            pos: (11, 10),
        },
        Question {
            idx: 50,
            clue: String::from("Coloring substance"),
            orientation: Orientation::Vertical,
            answer: vec!['d', 'y', 'e'],
            pos: (12, 10),
        },
    ]
}
