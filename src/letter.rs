pub struct Letter {
    pub inserted_char: Option<char>,
    pub desired_char: Option<char>,
    pub locked: bool,
    pub idx: Option<usize>,
    m_blocked: bool,
}

impl Default for Letter {
    fn default() -> Self {
        Self {
            inserted_char: None,
            desired_char: None,
            locked: false,
            idx: None,
            m_blocked: false,
        }
    }
}

impl Letter {
    pub fn get_string(&self) -> String {
        if self.inserted_char.is_some() {
            String::from(self.inserted_char.unwrap())
        } else {
            String::from("")
        }
    }

    pub fn toggle_lock(&mut self) {
        self.locked = !self.locked;
    }

    pub fn blocked(&self) -> bool {
        self.m_blocked || self.desired_char.is_none()
    }
}
